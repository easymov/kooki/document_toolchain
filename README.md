# Document toolchain

This document explain how to install and configure the toolchain to generate document at [Easymov](http://easymov.fr).

## Prerequisites

All the command executed below has been tested on an **Ubuntu 16.04**.

## Dependencies

- python3
- git
- texlive-full

### Installation

```
apt-get install python3-pip git texlive-full
```

## Tools

- [kooki](https://kooki.gitlab.io) : document generator
- [gener8](https://github.com/lelongg/gener8) : scafolding tool
- [konfiture](https://konfiture.gitlab.io) : speller and grammar checker

> Do not hesitate to check the documentation to understand how the tools works.

### Installation

```
pip3 install kooki gener8 konfiture
```

### Configuration

In a terminal you can type those command to configure kooki and gener8.

**Replace `<user>` by your username.**

```
export KOOKI_JAR_MANAGER="['https://gitlab.com/kooki/jar_manager/raw/master/jars.yml', 'https://gitlab.com/easymov/kooki/jar_manager/raw/master/jars.yml']"
export KOOKI_DIR="/home/<user>/.kooki/"
export GENER8_DIR="/home/<user>/.kooki/jars/"
```